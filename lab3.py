import turtle
def figura(a, kolor):
    wn = turtle.Screen();
    wn.bgcolor("white");
    t = turtle.Turtle();
    t.pensize(3);
    t.color(kolor);

    if(a==0):
        for i in range(2):
            t.forward(100);
            t.right(90);
            t.forward(50);
            t.right(90);
    elif(a==1):
        for i in range(3):
            t.forward(100);
            t.left(135);
    else:
        t.circle(50);

a = input("Podaj nr figury (0 - kw, 1 - tr, 3 - okr): ")
kolor =  raw_input("Podaj kolor figury: ")
figura(a, kolor);
